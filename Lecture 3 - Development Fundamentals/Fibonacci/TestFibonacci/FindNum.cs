using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fibonacci;
namespace TestFibonacci
{
    [TestClass]
    public class TestFindNumber
    {
        [TestMethod]
        public void TestFindNumber_WhenMatrixHasNumber()
        {
            int[,] matrix = {
            { 1, 2, 3 },
            { 5, 8, 13 },
            { 21, 34, 55 } };
            int expected = 8;

            int actual = FindNumberWithFibNeighbours.FindNumberWithFibonacciNeighbors(matrix);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestFindNumber_WhenMatrixHasNotNumber()
        {
            int[,] matrix = {
            { 1, 2, 95 },
            { 5, 8, 13 },
            { 21, 34, 55 } };
            int expected = -1;

            int actual = FindNumberWithFibNeighbours.FindNumberWithFibonacciNeighbors(matrix);
            Assert.AreEqual(expected, actual);
        }
    }
}