using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fibonacci;

namespace TestFibonacci
{
    [TestClass]
    public class TestFibSquare
    {
        [TestMethod]
        public void TestFibSquare_WhenMatrixWithSizeTwo_ReturnTrue()
        {
            int[,] matrix = { { 1, 1, }, { 3, 2 } };

            var result = FibonacciSquare.hasFibSeqInSquare(matrix);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestFibSquare_WhenMatrixWithSizeThree_ReturnTrue()
        {
            int[,] matrix = { { 1, 1, 2}, { 21, 4, 3 }, { 13, 8, 5 } };

            var result = FibonacciSquare.hasFibSeqInSquare(matrix);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestFibSquare_WhenMatrixWithSizeThree_ReturnFalse()
        {
            int[,] matrix = { { 1, 1, 5 }, { 21, 4, 3 }, { 13, 8, 5 } };

            var result = FibonacciSquare.hasFibSeqInSquare(matrix);

            Assert.IsFalse(result);
        }
    }
}
