namespace CSharpTasksDayTwoTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ArrangedIntArrayTestedRandomNumbers()
        {
            //Arrange
            int[] array = {0, 6, 111, 44, 2, 91 };
            int[] expected = { 0, 2, 6, 44, 91, 111 };
            CSharpTasksDayTwo tested = new CSharpTasksDayTwo();
            //Act
            int[] result = tested.ArrangeIntArray(array);
            //Assert
            CollectionAssert.AreEqual(expected,
            result);
        }
        public void ArrangedIntArrayTestedSmallNumbers()
        {
            //Arrange
            int[] array = { 0, 2, 1, 0, 3, 6 };
            int[] expected = { 0, 0, 2, 1, 3, 6 };
            CSharpTasksDayTwo tested = new CSharpTasksDayTwo();
            //Act
            int[] result = tested.ArrangeIntArray(array);
            //Assert
            CollectionAssert.AreEqual(expected,
            result);
        }
        public void ArrangedIntArrayTestedBigNumbers()
        {
            //Arrange
            int[] array = { 100, 221, 31, 1000, 332, 618 };
            int[] expected = { 100, 332, 31, 618, 221, 1000 };
            CSharpTasksDayTwo tested = new CSharpTasksDayTwo();
            //Act
            int[] result = tested.ArrangeIntArray(array);
            //Assert
            CollectionAssert.AreEqual(expected,
            result);
        }
    }
}
