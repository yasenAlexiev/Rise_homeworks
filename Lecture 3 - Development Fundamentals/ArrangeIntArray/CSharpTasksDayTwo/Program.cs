﻿// See https://aka.ms/new-console-template for more information
using System.Linq.Expressions;

public class CSharpTasksDayTwo
{
    static void Main() { 
    CSharpTasksDayTwo tested=new CSharpTasksDayTwo();
        int[] testArray = { 100, 221, 31, 1000, 332, 618 };
        int[]NewArray=tested.ArrangeIntArray(testArray);
        foreach (int element in NewArray)
        {
            Console.WriteLine(element);
        }
    }
    public int[] ArrangeIntArray(int[] array)
    {
        int[] bitsSumArray = new int[array.Length];
        int[] copyBitsSumArray = new int[array.Length];
        int[] rearangedArray = new int[array.Length];
        Array.Copy(array,rearangedArray, array.Length);
        foreach(int element in array) {
            string binaryString = Convert.ToString(element, 2);
            int binarySum = 0;
            foreach (char binaryDigit in binaryString)
            {
                if (binaryDigit == '1')
                {
                    binarySum++;
                }
            }
            int indexOfArray = Array.IndexOf(array, element);
            bitsSumArray[indexOfArray] = binarySum;
            copyBitsSumArray[indexOfArray] = binarySum;
        }
        Array.Sort(bitsSumArray,rearangedArray);
        return rearangedArray;
    }
}
